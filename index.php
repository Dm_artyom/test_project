<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Website</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="PopUp.js"></script>
    <script src="ajax.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="link_group">
    <a class="show_popup" rel="reg_form" href="#">Registration</a>
    <a class="show_popup" rel="log_in_form" href="#">Log in</a>
</div>
<!-- registration -->

<div class="popup reg_form">
    <a class="close" href="#">Close</a>
    <h2>Registration</h2>
    <form action="" method="post" id = "registration_form" name = "registration_form">

        <input name="login" type="text" id = "login" placeholder="Login" required><br />
        <input name="password" type="password" id = "password" placeholder="Password" required><br />
        <input name="confirm_passowrd" type="password" id = "confirm_passowrd" placeholder="Confirm password" required><br />
        <input name="email" type="email" id = "email" placeholder="Email" required><br />
        <input name="name" type="text" id = "name" placeholder="Name" required><br />

        <p class="register"><input class="button" id="register" name= "register" type="submit" value="Register"></p>
        <div id = "result_form_popup"></div>
    </form>
</div>
<div id = "result_form"></div>

<!--log in -->

<div class="popup log_in_form">
    <a class="close" href="#">Close</a>
    <h2>Log in</h2>
    <form action="" method="post" id = "log_in" name = "log_in" >

        <input name="login_1" type="text" id = "login_1" placeholder="Login" required><br />
        <input name="password_1" type="password" id = "password_1" placeholder="Password" required><br />


        <p class="login1"><input class="button" id="login1" name= "login1" type="submit" value="Log in"></p>
    </form>
</div>

</body>
</html>