$( document ).ready(function() {
    $("#register").click(
        function(e){
            e.preventDefault();
            sendAjaxForm('result_form', 'registration_form', 'http://localhost:81/registration.php');
            return false;
        }
    );
});

function sendAjaxForm(result_form, registration_form, url) {
    $.ajax({
        url: url,
        type: "POST",
        dataType: "JSON",
        data: $("#registration_form").serialize(),
        success: function (response) {
            // console.log(response);

            var result = JSON.parse(response);
            $('#result_form').html(result.name);
        },
        error: function (response) {
            $('#result_form').html("error");
        }
    });
}
